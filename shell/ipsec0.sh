#!/bin/bash

# 脚本运行计时。
SECONDS=0

CURR_DIR=/root
# shellcheck disable=SC2016
VPN_IPSEC_PSK='psk-eddie1223.'
VPN_USER='eddie'
VPN_PASSWORD='eddie1223.'

# 安装vpn l2tp/ipsec
echo "VPN_IPSEC_PSK=${VPN_IPSEC_PSK}
VPN_USER=${VPN_USER}
VPN_PASSWORD=${VPN_PASSWORD}" >> ${CURR_DIR}/vpn.env

# 没有 hwdsl2/ipsec-vpn-server 镜像，将会自动拉取镜像并实例化一个容器。
docker run \
    --name ipsec-vpn-server \
    --env-file ${CURR_DIR}/vpn.env \
    --restart=always \
    -v ${CURR_DIR}/ikev2-vpn-data:/etc/ipsec.d \
    -v /lib/modules:/lib/modules:ro \
    -p 500:500/udp \
    -p 4500:4500/udp \
    -d --privileged \
    hwdsl2/ipsec-vpn-server

# 查看日志，启动日志中有客户端连接必要的一些信息。（公网ip地址、用户名、密码、共享密钥）
docker logs ipsec-vpn-server

# 打印脚本执行时间
echo "Execution time: $SECONDS seconds"

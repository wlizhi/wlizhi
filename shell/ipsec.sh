#!/bin/bash

# 脚本运行计时。
SECONDS=0

CURR_DIR=/root
# shellcheck disable=SC2016
VPN_IPSEC_PSK='psk-eddie1223.'
VPN_USER='eddie'
VPN_PASSWORD='eddie1223.'
HAS_DOCKER="skip-docker"

if [ "$1" != "$HAS_DOCKER" ]; then
    echo "执行安装docker..."
    # 安装docker
    sudo yum remove docker \
                      docker-client \
                      docker-client-latest \
                      docker-common \
                      docker-latest \
                      docker-latest-logrotate \
                      docker-logrotate \
                      docker-engine

    sudo yum install -y yum-utils

    # 如果是国内的服务器，download.docker.com 是无法连接的，那么需要使用国内源来安装docker。
    # sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

    sudo yum install -y docker-ce

    sudo systemctl enable docker
    sudo systemctl start docker
fi

# 安装vpn l2tp/ipsec
echo "VPN_IPSEC_PSK=${VPN_IPSEC_PSK}
VPN_USER=${VPN_USER}
VPN_PASSWORD=${VPN_PASSWORD}" >> ${CURR_DIR}/vpn.env

# 没有 hwdsl2/ipsec-vpn-server 镜像，将会自动拉取镜像并实例化一个容器。
docker run \
    --name ipsec-vpn-server \
    --env-file ${CURR_DIR}/vpn.env \
    --restart=always \
    -v ${CURR_DIR}/ikev2-vpn-data:/etc/ipsec.d \
    -v /lib/modules:/lib/modules:ro \
    -p 500:500/udp \
    -p 4500:4500/udp \
    -d --privileged \
    hwdsl2/ipsec-vpn-server

# 查看容器内的 /etc/ipsec.d 目录的文件
#docker exec -it ipsec-vpn-server ls -l /etc/ipsec.d
# 示例：将一个客户端配置文件从容器复制到 Docker 主机当前目录
#docker cp ipsec-vpn-server:/etc/ipsec.d ${CURR_DIR}

# 查看日志，启动日志中有客户端连接必要的一些信息。（公网ip地址、用户名、密码、共享密钥）
docker logs ipsec-vpn-server

# 安装 vpn pptp。
#echo "# Secrets for authentication using PAP
#      # client    server      secret      acceptable local IP addresses
#      ${VPN_USER}    *           ${VPN_PASSWORD}    *" > /etc/ppp/chap-secrets
#docker run --name vpn-pptp -d --privileged --net=host -v /etc/ppp/chap-secrets:/etc/ppp/chap-secrets mobtitude/vpn-pptp


# 设置安全组，开放 udp 500 4500 端口。

# 打印脚本执行时间
echo "Execution time: $SECONDS seconds"
